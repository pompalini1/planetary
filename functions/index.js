// GLOBAL VARS
const PROJECT_ID = 'planetary-ffc29';
const SERVICE_ACCOUNT = './planetary-ffc29-firebase-adminsdk-648d2-2cdb11b2a2.json';
const API_KEY = 'AIzaSyAiGE7-JsTXS-BrcE2EHX9LA20fUB7aImc';
const SENDER_ID = '157428967781';
const AUTH_DOMAIN = `${PROJECT_ID}.firebaseapp.com`;
const DATABASE_URL = `https://${PROJECT_ID}.firebaseio.com`;
const STORAGE_BUCKET = `${PROJECT_ID}.appspot.com`;
const FB_CONFIG = {
  apiKey: API_KEY,
  authDomain: AUTH_DOMAIN,
  databaseURL: DATABASE_URL,
  projectId: PROJECT_ID,
  storageBucket: STORAGE_BUCKET,
  messagingSenderId: SENDER_ID
};

// Dependencies
const firebase = require('firebase');
const functions = require('firebase-functions');
const fs = require('fs');
const os = require('os');
const path = require('path');
const { Storage } = require('@google-cloud/storage');
const cors = require('cors')({ origin: true });
const Busboy = require('busboy');
const server = require('express')();

// Initialize the Realtime Database JavaScript SDK
firebase.initializeApp(FB_CONFIG);

// Get a reference to the database service
const database = firebase.database();

exports.upload = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    if (request.method !== 'POST') {
      return response.status(500).json({
        message: 'Not allowed'
      });
    }

    // parse the file with Busboy
    const busboy = new Busboy({ headers: request.headers });
    let uploadData = null;

    busboy.on('file', (fieldname, file, filename, enconding, mimetype) => {
      const filepath = path.join(os.tmpdir(), filename);
      uploadData = { file: filepath, type: mimetype, filename: filename };
      file.pipe(fs.createWriteStream(filepath));
    });

    busboy.on('finish', () => {
      const gcconfig = {
        projectId: PROJECT_ID,
        keyFilename: SERVICE_ACCOUNT
      }
      const storage = new Storage(gcconfig);
      const bucketName = STORAGE_BUCKET;
      const bucket = storage.bucket(bucketName);
      const file = bucket.file(uploadData.filename);

      bucket.upload(uploadData.file, {
        uploadType: 'media',
        metadata: {
          metadata: {
            contentType: uploadData.type
          }
        }
      }).then(() => {
        // get file url from storage api
        return getUploadedFileUrl(file, response);
      }).catch(error => {
        return response.status(500).json({
          error: error
        });
      });
    });

    return busboy.end(request.rawBody);
  });
});

getUploadedFileUrl = (file, response) => {
  return file.getSignedUrl({
    action: 'read',
    expires: '03-09-2491'
  })
  .then(urls => {
    return response.status(200).json({
      status: 'Ok',
      statusCode: 'Ok',
      statusText: 'Image uploaded',
      fileurl: urls[0]
    })
  });
}

fieldsToNumber = (data) => {
  // force the sizes data to be of type: Number
  data = data || {};
  data.diameter = data.diameter ? parseFloat(data.diameter) : null;
  data.distance_to_sun = data.distance_to_sun ? parseFloat(data.distance_to_sun) : null;
  data.orbital_period = data.orbital_period ? parseFloat(data.orbital_period).toFixed(2) : null;
  return Object.assign({}, data, data);
}

exports.planets = functions.https.onRequest((request, response) => {
  cors(request, response, () => {
    const planetsRef = '/sol/planets/';
    let data;
    let statusCode;

    if (request.method === 'GET') {
      statusCode = 200;

      // get full database snapshot without sorting
      database.ref(planetsRef).on('value', snapshot => {
        return response.status(statusCode).json({
          statusCode: statusCode,
          statusText: 'Ok',
          data: snapshot
        });
      });
    }

    // create a new planet using .set() so the id key comes from the frontend
    if (request.method === 'POST') {
      statusCode = 201;
      data = request.body;

      // timestamp is used to sort by date added
      data.timestamp = new Date().getTime();
      data = fieldsToNumber(data);

      database.ref(planetsRef).child(data.id).set(data, error => {
        if (error) {
          console.error(error);
        } else {
          return response.status(statusCode).json({
            statusCode: statusCode,
            statusText: 'Created',
            data: data
          });
        }
        return false;
      });
    }
    // update the planet with the given id
    if (request.method === 'PUT') {
      data = request.body;
      statusCode = 204;

      data = fieldsToNumber(data);

      database.ref(planetsRef + data.id).set(data, error => {
        if (error) {
          console.error(error);
        } else {
          return response.status(statusCode).json({
            statusCode: statusCode,
            statusText: 'Updated'
          });
        }
        return false;
      });
    }

    // delete the planet with the given id
    if (request.method === 'DELETE') {
      data = request.body;
      statusCode = 204;

      database.ref(planetsRef + data.id).remove(error => {
        if (error) {
          console.error(error);
        } else {
          return response.status(statusCode).json({
            statusCode: statusCode,
            statusText: 'Deleted'
          });
        }
        return false;
      });
    }
  });
});
