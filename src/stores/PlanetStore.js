import Actions from '../actions/Actions';

const INITIAL_STATE = {
  name: '',
  description: '',
  type: 'planet',
  orbital_period: '',
  distance_to_sun: '',
  diameter: '',
  orbits: 'Sun',
  color: '',
  file: {
    preview: '',
    url: ''
  }
}

export default class PlanetStore {
  constructor() {
    this.bindActions(Actions);
    this.planets = [];
    this.storedPlanets = [];
    this.formFields = {...INITIAL_STATE};
    this.formTitle = 'Add a';
    this.formAction = 'create';
    this.isLoading = true;
  }

  resetLoader = () => {
    this.setState({
      isLoading: true
    });
  }

  getPlanets = (planets) => {
    const planetsDescending = planets.reverse();

    this.setState({
      planets: planetsDescending
    });
    this.setState({
      storedPlanets: planetsDescending
    });
    this.setState({
      isLoading: false
    });
  }

  restorePlanets = () => {
    this.setState({
      planets: this.storedPlanets
    });
  }

  filterPlanetsByType = (type) => {
    let  filteredPlanets;

    if (type !== 'all') {
      filteredPlanets = this.storedPlanets.filter(planet => planet.type === type);
    } else {
      filteredPlanets = this.storedPlanets;
    }

    this.setState({
      planets: filteredPlanets
    });
  }

  filterPlanets = (text) => {
    this.cancelEditPlanet();
    this.restorePlanets();

    const filteredPlanets = this.planets.filter(planet => {
      const phrase = text.toLowerCase();
      const name = planet.name.toLowerCase();
      const description = planet.description.toLowerCase();
      const findInName = name.search('moon') >= 0 ? true : false;
      const findInDescription = description.indexOf(phrase) >= 0 ? true : false;

      if (findInName || findInDescription) {
        return planet;
      }
    });

    if (filteredPlanets.length) {
      this.setState({
        planets: filteredPlanets
      });
    }
  }

  createPlanet = (planet) => {
    // HACKY business, FIXME!!
    // Because we need to insert new planet(Object) at the begining of the array.
    // So first we need to reverse order and set to ascending
    // then concat the planet which appends to last position
    // and lastly reverse it so planets are in descending order again.
    this.setState({
      planets: this.planets.reverse().concat(planet).reverse()
    });
  }

  updatePlanet = (planetUpdated) => {
    // console.log(planetUpdated);

    this.setState({
      planets: this.planets.map(planet => {
        if (planet.id === planetUpdated.id) {
          return Object.assign(planet, planetUpdated);
        } else {
          return planet;
        }
      }),
      formFields: planetUpdated
    });

    this.cancelEditPlanet();
  }

  editPlanet = (id) => {
    const planet = this.planets.filter(planet => {
      if (planet.id === id) {
        return planet;
      }
    })[0];

    this.setState({
      formTitle: 'Edit',
      formAction: 'update',
      formFields: planet
    });
  }

  cancelEditPlanet = () => {
    window.scroll(0, 0);
    this.setState({
      formTitle: 'Add a',
      formAction: 'create',
      formFields: {
        name: '',
        description: '',
        type: 'planet',
        orbital_period: '',
        distance_to_sun: '',
        diameter: '',
        orbits: 'Sun',
        color: '',
        file: {
          preview: '',
          url: ''
        }
      }
    });
  }

  removeFile = () => {
    this.setState({
      formFields: {
        file: {
          data: '',
          preview: '',
          url: ''
        }
      }
    });
  }

  deletePlanet = (id) => {
    this.setState({
      planets: this.planets.filter(planet => planet.id !== id)
    });
    this.cancelEditPlanet();
  }
}
