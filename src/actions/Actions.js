import alt from '../libs/alt';

export default alt.generateActions(
  'getPlanets',
  'createPlanet',
  'updatePlanet',
  'editPlanet',
  'cancelEditPlanet',
  'removeFile',
  'getPlanets',
  'filterPlanets',
  'filterPlanetsByType',
  'restorePlanets',
  'deletePlanet',
  'resetLoader',
);
