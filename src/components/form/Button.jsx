import React, { Component } from 'react';


class Button extends Component {
  setButtonText = () => {
    if (this.props.action === 'create') {
      return `${this.props.title} ${this.props.text}`
    } else if (this.props.action === 'update'){
      return `Save ${this.props.text}`;
    } else {
      return this.props.text;
    }
  }

  render() {
    return(
      <button {...this.props}>{this.setButtonText()}</button>
    );
  }
}

export default Button;
