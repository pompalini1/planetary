import React, { Component } from 'react';
import Button from '../form/Button';
import ImageUploader from 'react-images-upload';
import { TwitterPicker } from 'react-color';
import './Form.sass';



class Form extends Component {
  constructor(props) {
    super(props);
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
  }

  state = {
    formTitle: this.props.formTitle,
    type: this.props.formFields.type,
    formAction: this.props.formAction,
    formFields: this.props.formFields,
    pickerVisible: false
  };

  UNSAFE_componentWillReceiveProps = (prevProps) => {
    this.setState({
      formFields: Object.assign(this.state.formFields, prevProps.formFields),
      formTitle: prevProps.formTitle,
      formAction: prevProps.formAction
    });
  }

  UNSAFE_componentWillMount = () => {
    document.addEventListener('mouseup', this.handleClickOutside);
  }

  UNSAFE_componentWillUnmount = () => {
    document.addEventListener('mouseup', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        pickerVisible: false
      });
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    // console.log(this.state.formAction);

    this.state.formAction === 'create' ?
      this.createPlanet() :
      this.updatePlanet();
  }

  updatePlanet = () => {
    this.props.onUpdate(this.state.formFields);
  }

  createPlanet = () => {
    this.props.onCreate(this.state.formFields);
  }

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.innerHTML || target.value;
    const key = target.id || target.name;
    const formFields = this.state.formFields;

    formFields[key] = value;

    this.setState({
      formFields: formFields
    }, () => { this.validateField(key, value) });

  }

  validateField = (fieldName, value) => {
    const fieldValidationErrors = this.state.formErrors;
    let nameValid = this.state.nameValid;

    switch(fieldName) {
      case 'name':
        nameValid = value.length > 0;
        break;
      default:
        break;
    }

    this.setState({
      formErrors: fieldValidationErrors,
      nameValid: nameValid
    }, this.validateForm);
  }

  validateForm = () => {
    this.setState({formValid: this.state.passwordValid});
  }

  getTypeText = () => {
    return `Add ${this.props.type}`;
  }

  handleTypeChange = (event) => {
    const type = event.target.value;
    const formFields = this.state.formFields;

    formFields.type = type;

    this.setState({
      type: type,
      formFields: formFields
    });
  }

  handleFileChange = (file) => {
    // create an image preview with blob
    const formFields = this.state.formFields;
    formFields.file.preview = URL.createObjectURL(file[0]);
    formFields.file.data = file[0];
    this.setState({
      formFields: formFields
    });
    console.log(this.state.formFields);

  }

  handleFileRemove = () => {
    // reset the file fields
    this.setState({
      formFields: {
        file: {
          data: '',
          preview: '',
          url: ''
        }
      }
    });
  }

  handleColorClick = (event) => {
    this.setState({
      pickerVisible: true
    });
  }

  handleColorChange(color, event) {
    const formFields = this.state.formFields;
    formFields.color = color.hex;
    this.setState({
      formFields: Object.assign({}, this.state.formFields, formFields)
    });
  }



  render() {
    const fields = this.state.formFields;
    const hasPreview = fields.file && fields.file.preview && fields.file.preview.length ? true : false;
    const hasUrl = fields.file && fields.file.url && fields.file.url.length ? true : false;
    const isPickerVisible = this.state.pickerVisible;

    const ColorPicker = () => {
      if (isPickerVisible) {
        return (
          <div className="color-picker" ref={this.setWrapperRef}>
            <TwitterPicker
              color={this.state.formFields.color}
              onChangeComplete={this.handleColorChange}/>
          </div>
        );
      } else {
        return null;
      }
    }

    const Image = () => {
      if (hasUrl || hasPreview) {
       return (
         <figure className="image-preview">
           <img src={(!fields.file.url && fields.file.preview) || fields.file.url} className="image" />
           <button
            className="btn btn-remove"
             onClick={this.props.onRemoveFile}>
              x
            </button>
         </figure>
       )
     } else {
       return (
         <div className="image-uploader">
           <ImageUploader
             name='myFile'
             whithPreview={true}
             withIcon={false}
             buttonText='Upload image'
             buttonClassName='btn'
             onChange={this.handleFileChange}
             imgExtension={['.jpg', '.jpeg', '.gif', '.png', '.gif']}
             singleImage={true} />
         </div>
       )
     }
    };

    return (
      <form className="form" id="form_id" onSubmit={this.handleSubmit}>
        <h2>
          {this.props.formTitle}
          <select
            id="type_select"
            value={fields.type || this.state.type}
            onChange={this.handleTypeChange}>
              <option value="planet">Planet</option>
              <option value="moon">Moon</option>
          </select>
        </h2>
        <Image/>
        <label>Name:</label>
        <input
          name="name"
          value={fields.name}
          className="form-input"
          type="text"
          onChange={this.handleInputChange}
          required/>
        <label>Description:</label>
        <input
          name="description"
          value={fields.description}
          className="form-input"
          type="text"
          onChange={this.handleInputChange}
          required/>
        <div className="form-control inline">
          <label>Diameter (km):</label>
          <input
            name="diameter"
            value={fields.diameter}
            className="form-input"
            type="number"
            onChange={this.handleInputChange}/>
        </div>
        <div className="form-control inline">
          <label>Distance to sun (10<sup>6</sup> km):</label>
          <input
            name="distance_to_sun"
            value={fields.distance_to_sun}
            className="form-input"
            type="number"
            onChange={this.handleInputChange}/>
        </div>
        <div className="form-control inline">
          <label>Orbital period(days):</label>
          <input
            name="orbital_period"
            value={fields.orbital_period}
            className="form-input"
            type="number"
            onChange={this.handleInputChange}/>
        </div>
        <div className="form-control inline">
          <label>Color (#hex):</label>
          <ColorPicker />
          <input
            autoComplete="off"
            name="color"
            value={fields.color}
            className="form-input"
            type="text"
            onClick={this.handleColorClick}
            onChange={this.handleInputChange} />
        </div>
        <div className="actions">
        <Button
          title={this.state.formTitle}
          action={this.state.formAction}
          text={this.state.formFields.type || this.state.type}
          type="submit"
          className='btn btn-add'
          onSubmit={this.handleSubmit}/>
        <Button
          title="Cancel"
          text="Cancel"
          type="button"
          className='btn btn-add'
          onClick={this.props.onCancelEdit}/>
        </div>
      </form>
    );
  }
}

export default Form;

