import './App.sass';
import React, {Component} from 'react';
import Planets from './planets/PlanetsList';
import Planets2D from './planets/Planets2D';
import NameForm from './form/Form';
import Button from './form/Button';
import Actions from '../actions/Actions';
import Menu from './menu/Menu';
import connect from '../libs/connect';
import fetchData from '../libs/fetch';
import uuidv4 from 'uuid/v4';
import FirebaseConnector from '../libs/FirebaseConnector';


class App extends Component {

  state = {
    formFields: {},
    formAction: '',
    formTitle: '',
    viewDashboard: false
  };

  componentDidMount = () => {
    const orderBy = this.state.viewDashboard ? 'timestamp' : 'distance_to_sun';
    this.getPlanets(orderBy);
  };

  handleChangeView = () => {
    const orderBy = this.state.viewDashboard ? 'distance_to_sun' : 'timestamp';

    this.setState({
      viewDashboard: !this.state.viewDashboard
    });

    this.getPlanets(orderBy);
  }

  handleFilterClick = (type) => {
    this.props.Actions.filterPlanetsByType(type);
  }

  handleInputChange = (text) => {
    // do something with the search input inserted text
    if (text.length >= 3) {
      this.filterPlanets(text);
    } else {
      this.restorePlanets(text);
    }
  }

  getPlanets = (orderBy = 'timestamp') => {
    let planets = [];
    const database = new FirebaseConnector('/sol/planets/');
    this.props.Actions.resetLoader();

    database.orderByChild(orderBy).once('value', snapshot => {
      snapshot.forEach(function (childSnapshot) {
        const data = childSnapshot.val();
        let planet = {};
        planet.id = childSnapshot.key;
        Object.assign(planet, data);
        planets.push(planet);
      });

      this.props.Actions.getPlanets(planets);
    });
  }

  filterPlanets = (text) => {
    this.props.Actions.filterPlanets(text);
  }

  restorePlanets = (text) => {
    this.props.Actions.restorePlanets(text);
  }

  editPlanet = (id) => {
    this.props.Actions.editPlanet(id);
  }

  cancelEditPlanet = () => {
    this.props.Actions.cancelEditPlanet();
  }

  removeFile = () => {
    this.props.Actions.removeFile();
  }

  updatePlanet = (data, isUpload) => {
    const file = data.file || {};
    if (!data) {
      throw new Error('data is not defined');
    }

    // console.log('data before put:', data);
    // console.log(isUpload);


    fetchData({
      url: 'planets/',
      method: 'PUT',
      data: data
    }).then(response => {
      if (file && file.data && !isUpload) {
        this.handleFileUpload(data);
      } else {
        this.props.Actions.updatePlanet(data);
      }
    });
  }

  deletePlanet = (id) => {
    if (!id) {
      throw new Error('id is not defined');
    }
    const data = {
      id: id
    }

    fetchData({
      url: 'planets/',
      method: 'DELETE',
      data: data
    }).then(response => {
      this.props.Actions.deletePlanet(id);
    })
  }

  createPlanet = (data) => {
    const file = data.file || {};
    if (!data) {
      throw new Error('data is not defined');
    }

    data.id = 'planet-' + uuidv4();

    // console.log('data before create:', data);

    fetchData({
      url: 'planets/',
      method: 'POST',
      data: data
    }).then(response => {
      this.props.Actions.createPlanet(response.data);
      if (file && file.data) {
        this.handleFileUpload(data);
      }
    });
  }

  handleFileUpload = (data) => {
    const imageData = new FormData();
    imageData.append('image', data.file.data);
    // upload the file
    fetchData({
      url: 'upload/',
      method: 'POST',
      data: imageData,
      dataType: 'formData'
    })
    .then(response => {
      data.file.url = response.fileurl;
      this.updatePlanet(data, true);
    })
    .catch(error => console.error('Upload Failed:', error));
  }

  render() {
    const { planets, formFields, formTitle, formAction, isLoading } = this.props;

    if (isLoading) {
      return (
        <div className="loader">
          <div className="ripple">
            <div></div><div></div>
          </div>
        </div>
      )
    } else if (this.state.viewDashboard) {
      return (
        <div className="container dashboard">
          <div className="left">
            <NameForm
              formAction={formAction}
              formTitle={formTitle}
              formFields={formFields}
              onUpdate={this.updatePlanet}
              onCancelEdit={this.cancelEditPlanet}
              onRemoveFile={this.removeFile}
              onCreate={this.createPlanet} />
          </div>
          <div className="right">
            <Menu
              onFilterClick={this.handleFilterClick}
              onInputChange={this.handleInputChange} />
            <Planets
              planets={planets}
              onPlanetDelete={this.deletePlanet}
              onPlanetEdit={this.editPlanet} />
          </div>
          <div className="footer">
            <Button
              className="btn btn-lg"
              onClick={this.handleChangeView}
              text="2D View" />
          </div>
        </div>
      );
    } else {
      return (
        <div className="container">
          <Planets2D
            planets={planets} />
          <div className="footer">
            <Button
              className="btn btn-lg"
              onClick={this.handleChangeView}
              text="Dashboard" />
          </div>
        </div>
      );
    }
  }
}

export default connect(({ planets, formFields, formTitle, formAction, isLoading}) => ({
  planets,
  formFields,
  formTitle,
  formAction,
  isLoading
}), {
  Actions
})(App)
