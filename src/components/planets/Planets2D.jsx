import React, { Component } from 'react';
import Planet from './Planet';

import './Planets2D.sass';


export default class Planets2D extends Component {
  state = {
    zoomIndex: 2,
    zoomValues: [
      0.05,
      0.25,
      0.75,
      2,
      4,
    ],
  }

  UNSAFE_componentWillMount = () => {
    this.setState({
      zoomFactor: this.state.zoomValues[this.state.zoomIndex]
    });
  };

  handleZoomIn = (event) => {
    let value = this.state.zoomIndex + 1;

    if (value >= this.state.zoomValues.length - 1) {
      value = this.state.zoomValues.length - 1;
    }

    this.setState({
      zoomIndex: value,
      zoomFactor: this.state.zoomValues[value]
    }, () => {
      this.toggleButton();
    });

    this.resetRotationAnim();
  }

  handleZoomOut = (event) => {
    let value = this.state.zoomIndex - 1;

    if (value <= 0){
      value = 0;
    }

    this.setState({
      zoomIndex: value,
      zoomFactor: this.state.zoomValues[value]
    }, () => {
      this.toggleButton();
    });

    this.resetRotationAnim();
  }

  toggleButton = () => {
    const currentIndex = this.state.zoomIndex;
    const buttons = document.querySelectorAll('.btn-zoom');

    // first enable all
    buttons.forEach(button => {
      button.removeAttribute('disabled');
    });

    // disable zoom out
    if (currentIndex <= 0) {
      buttons[1].setAttribute('disabled', '');
    }

    // disable zoom in
    if (currentIndex >= this.state.zoomValues.length - 1) {
      buttons[0].setAttribute('disabled', '');
    }
  }

  resetRotationAnim = () => {
    const planets = document.querySelectorAll('.planets-2d-item');
    planets.forEach(element => {
      const orbit = element.childNodes[0];
      const planet = orbit && orbit.childNodes[0];
      if (!planet) {
        return;
      }
      const canZoom = planet.classList.contains('can-zoom');
      planet.classList.add('hide');
      planet.classList.remove('rotate');
      planet.classList.remove('can-zoom');
      // element.classList.remove('hide');

      setTimeout(() => {
        const width = orbit.offsetWidth;

        if (width >= 57) {
          planet.classList.remove('hide');
        } else {
          element.classList.add('hide');
        }

        if (canZoom) {
          planet.classList.add('can-zoom');
        }

        planet.classList.add('rotate');

      }, 1000);
    });
  }

  render() {
    const zoomFactor = this.state.zoomFactor;
    const sun_width = 40 * zoomFactor;

    const planets = this.props.planets.map(({id, name, description, file, diameter, distance_to_sun, orbital_period, type, color}) => {
      const planetClassName = ' ' + name.toLowerCase();
      const orbit = (distance_to_sun * 2) * zoomFactor;
      const planet_width = (diameter / 1000) * zoomFactor;
      const orbit_amin = orbit / 2;
      const margin = (orbit_amin - (planet_width / 2));
      const isPlanet = type === 'planet' && orbit !== 0;
      const canZoom = planet_width < 30 ? ' can-zoom' : '';
      const isHidden = orbit < 57 ? ' hide' : '';

      // console.log(name, orbit);


      orbital_period = parseFloat(orbital_period / 10).toFixed(2);
      // console.log('name:', name, 'diameter', diameter, 'orbit:', orbit, 'orbital_period:', orbital_period);

      if (isPlanet) {
        return (
          <Planet
            key={id}
            className={'planets-2d-item' + planetClassName + canZoom + isHidden}>
            <div
              className="orbit"
              style={{
                width: orbit + 'px',
                height: orbit + 'px',
                '--orbit-anim': orbit_amin + 'px',
                '--orbital-period': orbital_period + 's',
                }}>
              <div
                className="rotate planet"
                style={{
                  '--planet-width': planet_width + 'px',
                  '--planet-color': color,
                  '--planet-margin': margin + 'px'}}>
                  {/* <div className="satellite-orbit">
                    <div className="satellite">
                      <div className="moon"></div>
                    </div>
                  </div> */}
              </div>
            </div>
            <div className="title">{name}</div>
          </Planet>
        );
      }
    });

    return (
      <div>
        <ul className='planets-2d'>
          <li className="planets-2d-item sol" style={{
            '--sun-width': sun_width + 'px',
            '--sun-width-n': '-' + sun_width + 'px',
          }}></li>
          {planets}
        </ul>
        <div className="actions">
          <button className="btn btn-zoom in" onClick={this.handleZoomIn}>+</button>
          <button className="btn btn-zoom out" onClick={this.handleZoomOut}>-</button>
        </div>
      </div>
    )
  }
}
