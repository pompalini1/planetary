import React, { Component } from 'react';
import Planet from './Planet';
import Button from '../form/Button';
import Pagination from '../pagination/Pagination';

import './PlanetsList.sass';

export default class Planets extends Component {

  state = {
    planets: [],
    pageOfItems: [],
    pageSize: 7,
    initialPage: 1,
    viewAsTable: true
  }

  handleSwitchListType = () => {
    let pageSize;
    this.setState({
      viewAsTable: !this.state.viewAsTable
    }, () => {
      if (this.state.viewAsTable) {
        pageSize = 7;
      } else {
        pageSize = 4;
      }
      this.setState({
        pageSize: pageSize
      });
    });
  }

  onChangePage = (pageOfItems) => {
    // update state with new page of items
    this.setState({ planets: pageOfItems, pageOfItems: pageOfItems });
  }

  handlePlanetDelete = (id) => {
    // forcing the state change here to supress a duplicate key error
    this.setState({
      planets: this.state.planets.filter(planet => {
        return planet.id !== id
      })
    }, () => {
      this.props.onPlanetDelete(id);
    });
  }

  componentDidUpdate = () => {
    // prevent getting into a loop here
    if (this.state.pageOfItems.length) {
      return;
    }
    if (this.props.planets.length != this.state.planets.length) {
      this.setState({
        planets: Object.assign(this.state.planets, this.props.planets)
      });
    }
  }

  componentDidMount = () => {
    if (this.props.planets.length != this.state.planets.length) {
      this.setState({
        planets: Object.assign(this.state.planets, this.props.planets)
      });
    }
  }

  render() {

    let pages;


    if (this.state.planets.length) {
      pages = <Pagination
        items={this.props.planets}
        pageSize={this.state.pageSize}
        onChangePage={this.onChangePage} />;
    }

    const viewAsTableClass = this.state.viewAsTable ? ' table-view' : '';
    const viewAsTableText = this.state.viewAsTable ? 'list' : 'table';

    // Display paginated planets
    const indexOfLastPlanet = this.state.initialPage * this.state.pageSize;
    const indexOfFirstPlanet = indexOfLastPlanet - this.state.pageSize;
    const pagedPlanets = this.state.planets.slice(indexOfFirstPlanet, indexOfLastPlanet);

    // console.log('pageSize:', this.state.pageSize);
    // console.log('pagedPlanets', pagedPlanets);
    // console.log('planets on render', this.state.planets);

    const planets = pagedPlanets.map(({ id, name, description, file, diameter, distance_to_sun, orbital_period, color }) => {
      return (
        <Planet
          key={id}
          className="planets-list-item">
          <figure className="image-container">
            <img src={file ? file.url: ''} className="image" />
          </figure>
          <div className="content-container">
            <h4 className="name">{name}</h4>
            <div className="description-container">
              <p className="description">{description}</p>
            </div>
            <p className="diameter field">
              <span className="field-name">Diameter:</span>
              <br/>
              {diameter}
              <span className="field-name"> km</span>
            </p>
            <p className="distance field">
              <span className="field-name">Distance to sun:</span>
              <br />
              {distance_to_sun}
              <span className="field-name"> x 10<sup>6</sup> km</span>
            </p>
            <p className="orbital-period field">
              <span className="field-name">Orbital period:</span>
              <br />
              {orbital_period}
              <span className="field-name"> days</span>
            </p>
          </div>
          <div className="actions">
            <Button
              className='btn btn-edit'
              text="Edit"
              onClick={this.props.onPlanetEdit.bind(null, id)} />
            <Button
              className='btn btn-remove'
              text="Delete"
              onClick={this.handlePlanetDelete.bind(null, id)} />
          </div>
        </Planet>
      );
    });

    return (
      <div className={'planets-list-content' + viewAsTableClass}>
        <div className="header">
          <div className="table-head">
            <div className="name">Name:</div>
            <div className="description">Description:</div>
            <div className="diameter">Diameter <br/> (km):</div>
            <div className="distance">Distance to sun <br/> (10<sup>6</sup> km):</div>
            <div className="orbital-period">Orbital period <br/> (days):</div>
          </div>
          <div className="actions">
            <button
              className="btn"
              onClick={this.handleSwitchListType}>
              View as {viewAsTableText}
            </button>
          </div>
        </div>
        <div id="scroller" className="planets-list-container">
          <ul className='planets-list'>
            {planets}
          </ul>
        </div>
        {pages}
      </div>
    )
  }
}
