import React, { Component } from 'react';


export default ({children, ...props}) => (
  <li {...props}>
    {children}
  </li>
)
