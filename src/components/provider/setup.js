import PlanetStore from '../../stores/PlanetStore';


export default alt => {
  alt.addStore('PlanetStore', PlanetStore);
}
