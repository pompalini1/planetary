import React, { Component } from 'react';
import Button from '../form/Button';

import './Menu.sass';


export default class Menu extends Component {
  state = {
    selectedFilter: 'all'
  }

  handleInputChange = (event) => {
    this.props.onInputChange(event.target.value);
  }

  handleFilterClick = (event) => {
    const type = event.target.getAttribute('data-type').toLowerCase();

    this.setState({
      selectedFilter: type
    })
    this.props.onFilterClick(type);
  }

  render() {
    const buttonDefaultClass = 'btn btn-edit';
    const selected = this.state.selectedFilter;

    return (
      <div className="menu">
        <div className="left">
          <input
            type="text"
            className="search"
            onChange={this.handleInputChange}
            placeholder="Search"/>
        </div>
        <div className="right">
          <div className="filters">
            <span>show:</span>
            <Button
              className={buttonDefaultClass + (selected === 'all' ? ' active' : '')}
              onClick={this.handleFilterClick}
              text="All"
              data-type="all"
              title="All" />
            <Button
              className={buttonDefaultClass + (selected === 'planet' ? ' active' : '')}
              onClick={this.handleFilterClick}
              text="Planets"
              data-type="planet"
              title="Planets" />
            <Button
              className={buttonDefaultClass + (selected === 'moon' ? ' active' : '')}
              onClick={this.handleFilterClick}
              text="Moons"
              data-type="moon"
              title="Moons"/>
          </div>
        </div>
      </div>
    )
  }
}
