const DB_URL = 'https://us-central1-planetary-ffc29.cloudfunctions.net/';


export default (options) => {

  options.url = DB_URL + options.url || 'planets/';
  options.method = options.method || 'GET';
  options.dataType = options.dataType || 'json';

  const settings = {
    method: options.method, // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, cors, *same-origin
    headers: {}
  }

  if (options.data && options.method !== 'GET') {
    settings.body = options.dataType === 'json' ? JSON.stringify(options.data) : options.data;
  }

  if (options.data && options.method === 'DELETE') {
    settings.body = JSON.stringify(options.data);
  }

  if (options.data && options.method === 'PUT') {
    settings.body = JSON.stringify(options.data);
  }

  if (options.dataType === 'json') {
    settings.headers['Content-Type'] = 'application/json; charset=utf-8';
  }


  return fetch(options.url, settings).then(response => {
    if (settings.method === 'GET' || settings.method === 'POST') {
      // parse response to JSON
      return response.json();
    } else {
      return response;
    }
  });
}
