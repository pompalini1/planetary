import firebase from 'firebase/app';
import 'firebase/database';

// Initialize Firebase API
const config = {
  apiKey: "AIzaSyAiGE7-JsTXS-BrcE2EHX9LA20fUB7aImc",
  authDomain: "planetary-ffc29.firebaseapp.com",
  databaseURL: "https://planetary-ffc29.firebaseio.com",
  projectId: "planetary-ffc29",
  storageBucket: "planetary-ffc29.appspot.com",
  messagingSenderId: "157428967781"
};
firebase.initializeApp(config);


export default class FirebaseConnector {
  constructor(db_reference) {
    const database = firebase.database();
    return database.ref(db_reference);
  }
}
