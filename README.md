# Planetary

Watch a [Live Demo](https://planetary-ffc29.firebaseapp.com)

## Build pattern used:
Webpack 4 > React 16 + Alt(Flux) with ES6, SASS and hosted with Firebase.

## How to run it:
Assuming that [Node](https://nodejs.org/) (>= 8.9.4) is installed and this is a requirement;
Use [Node Version Manager](https://github.com/creationix/nvm) to change node's versions if necessary.

Clone:
```shell
$ git clone https://gitlab.com/pompalini1/planetary.git
```
Or [Download the project](https://gitlab.com/pompalini1/planetary/-/archive/master/planetary-master.zip)

After and inside the cloned `planetary` or the unziped `planetary-master` folder

Run:
```shell
$ npm install && npm start
```

When the build finishes the browser should open with [http://localhost:3000](http://localhost:3000)
